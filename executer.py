import logging
import os
import subprocess
from datetime import datetime, timedelta, timezone
from time import sleep

import requests
from dotenv import load_dotenv
from peewee import DoesNotExist
from raven.handlers.logging import SentryHandler

from article import post_wall_post_with_article, is_article_link, convert_article_link_to_id
from models import db, Post, Type, Info
from my_exceptions import ImproperlyConfigured, BadTime, BadPublic, SelfRepost
from my_functions import get_admin_pages, find_public, load_image, write_data_to_files, check_post, \
    read_from_file, \
    file_exists, clean_chrome_cache, get_posts_number, create_report_text, get_owner_id_from_link, \
    create_post_text
from send_message import MessageSender
from vk_api import VkRequest, VkException


def run():
    load_dotenv(os.path.join(os.path.dirname(os.path.abspath(__file__)), '.env'), verbose=True)
    if int(os.environ.get('OS_DEBUG')):
        exit(0)
    db.connect()

    added_type = Type.get(Type.text == 'Added')
    working_type = Type.get(Type.text == 'Working')

    if (Post.select().where(
                (Post.type == added_type) & (Post.attempts <= 1)
    ).exists() and
            not Post.select().where(Post.type == working_type).exists()):
        tg_bot_token = os.environ.get('TG_BOT_TOKEN')
        admin_tg_id = os.environ.get('ADMIN_TG_ID')
        sikulix_dir = os.environ.get('SIKULIX_DIR')
        image_dir = os.environ.get('IMAGE_DIR')
        clicker_log_dir = os.environ.get('CLICKER_LOG_DIR')

        post = Post.get(Post.type == added_type & (Post.attempts <= 1))
        post.type = 2
        post.attempts += 1
        post.save()
        try:
            user_info = Info.get(Info.user == post.user)
            access_token = user_info.access_token
            vk_id = user_info.vk_id
            tg_id = user_info.tg_id
            sender = MessageSender(tg_bot_token, user_info.notify_type.get_id(), tg_id, admin_tg_id)
        except DoesNotExist:
            post.type = 3
            post.save()
            raise ImproperlyConfigured('Пользователь добавил пост, не указав access_token')

        post_id = post.get_id()
        post_link = f"http://{os.environ.get('DOMAIN')}/app/post/{post_id}"
        post_title_text = create_post_text(post)

        now_obj = datetime.now(timezone(timedelta(hours=3)))
        month, day = str(now_obj.month), str(now_obj.day)
        logging_dir = os.path.join(os.environ.get('LOGGING_PATH'), 'Reposter', month, day)
        if not os.path.exists(logging_dir):
            os.makedirs(logging_dir)
        logging_file = os.path.join(logging_dir, 'logging.log')
        logging_level = os.environ.get('LOGGING_LEVEL', 'INFO')
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        file_handler = logging.FileHandler(logging_file)
        file_handler.setLevel(getattr(logging, logging_level.upper(), logging.INFO))
        formatter = logging.Formatter(
            '%(asctime)s %(name)s %(levelname)s: %(message)s',
            datefmt='%d.%m.%Y %H:%M')
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        if not int(os.environ.get('DEBUG', 0)):
            sentry_handler = SentryHandler(os.environ.get('SENTRY_DSN'))
            sentry_handler.setLevel(logging.ERROR)
            logger.addHandler(sentry_handler)

        # noinspection PyBroadException
        try:
            try:
                publics_where_user_is_admin = get_admin_pages(vk_id, access_token)
            except VkException as e:
                post.type = 3
                post.save()
                sender(f"Возникла ошибка связи с ВК😔\n\n[Пост]({post_link}): {str(e)}",
                       event_type=3, notify_admin=False)
                raise
            try:
                public_id = find_public(post.public, publics_where_user_is_admin)
            except KeyError:
                post.type = 3
                post.save()
                sender("Ошибка😔\n\n"
                       f"Неверный паблик для [поста]({post_link}) {post_title_text}: \"{post.public}\""
                       "либо у тебя нету права постить в нем.\n"
                       "Пожалуйста, указывай максимально длинное имя паблика, "
                       "чтобы я был уверен, куда постить и где проверять. Спасибо😌",
                       event_type=3, notify_admin=True,
                       debug_info={'post-id': post_id,
                                   'public': post.public,
                                   'user-publics': publics_where_user_is_admin})
                raise BadPublic('Неверное имя паблика')

            r = VkRequest(access_token, 5.75)
            backup_delta = timedelta(minutes=2)
            utc = timezone(timedelta(hours=0))
            if not post.datetime.astimezone(tz=utc) > datetime.now(tz=utc) + backup_delta:
                post.type = 3
                post.save()
                sender("Ошибка😔\n\n"
                       f"Время [поста]({post_link}) {post_title_text}"
                       f" больше текущего на менее чем 2 минуты (либо вообще меньше)\n\n"
                       "Поэтому я не буду его откладывать, т.к. боюсь не успеть.\n"
                       "Создай, пожалуйста, новый пост. Спасибо😌",
                       event_type=3, notify_admin=False)
                raise BadTime('Время поста уже прошло')

            try:
                image_path = load_image(post.image, image_dir)
            except requests.RequestException:
                sender(f"Ошибка😔\n\n"
                       f"Неверная ссылка на картинку [поста]({post_link}) {post_title_text}.\n"
                       f"Проверь ее вручную в браузере: \"{post.image}\"\n\n"
                       f"А затем создай, пожалуйста, новый пост. Спасибо😌",
                       event_type=3, notify_admin=False)
                post.type = 3
                post.save()
                raise
            if is_article_link(post.link):
                article_id = convert_article_link_to_id(post.link)
                try:
                    post.new_post_id = post_wall_post_with_article(user_info.access_token, public_id,
                                                                   post.text, image_path, article_id,
                                                                   post.datetime)
                except VkException as e:
                    post.type = 3
                    post.save()
                    if 'Access to adding post denied: a post is already scheduled for this time' \
                            in str(e):
                        sender(f"Ошибка 😔\n\nНа время твоего [поста]({post_link}) уже "
                               f"запланирован другой пост", event_type=3, notify_admin=False)
                    else:
                        sender(f"Возникла ошибка при постинге статьи😔\n\n[Пост]({post_link}): {str(e)}",
                               event_type=3, notify_admin=True)
                    raise
                post.type = 4
                title = post.text.splitlines()[0]
                post_vk_link = f'https://vk.com/wall-{post.new_post_id}'
                text_minsk_time = post.datetime.astimezone(timezone(timedelta(hours=3))).strftime("%d.%m %H:%M")
                sender(
                    f"[Пост]({post_link}) *{title}* успешно отложен в *{post.public}* на *{text_minsk_time}* "
                    f"✅\n[Открыть пост ВК]({post_vk_link})",
                    event_type=2, notify_admin=False)
            else:
                posts_public_id = get_owner_id_from_link(post.link)
                if posts_public_id == -public_id:
                    post.type = 3
                    post.save()
                    sender("Ошибка😔\n\n"
                           f"[Пост]({post_link}) {post_title_text} репоститься в свой же паблик.\n"
                           "Так нельзя", event_type=3, notify_admin=False)
                    raise SelfRepost

                write_data_to_files(post, sikulix_dir, user_info.profile_dir)
                os.environ['DISPLAY'] = ':0'
                chrome = subprocess.Popen('exec google-chrome-stable --no-sandbox',
                                          shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                sikulix = subprocess.run('/home/dimadk/Sikulix/runsikulix '
                                         '-r /home/dimadk/Sikulix_Project/repost_script.sikuli',
                                         shell=True, stdout=subprocess.PIPE, universal_newlines=True)
                chrome.terminate()
                sleep(2)  # fixes issue: REPOSTER-C
                clean_chrome_cache()

                with open(clicker_log_dir + 'executer.log', 'w', encoding='utf8') as f:
                    f.write(sikulix.stdout)

                if file_exists(clicker_log_dir + 'success.log'):
                    try:
                        verified, data = check_post(post, public_id, access_token)
                    except VkException as e:
                        post.type = 3
                        post.save()
                        sender(f'При проверке [поста]({post_link}) {post_title_text} возникла ошибка 😔:'
                               f'{e.args}',
                               event_type=2, notify_admin=True,
                               debug_info={
                                   'post-id': post_id,
                                   'e.args': e.args
                               })
                        raise
                    if verified:
                        post.new_post_id = data
                        post.type = 4
                        title = post.text.splitlines()[0]
                        post_vk_link = 'https://vk.com/wall' + data
                        text_minsk_time = post.datetime.astimezone(timezone(timedelta(hours=3))).strftime("%d.%m %H:%M")
                        sender(f"[Пост]({post_link}) *{title}* успешно отложен в *{post.public}* на *{text_minsk_time}* "
                               f"✅\nЯ его проверил, все верно\n[Открыть пост ВК]({post_vk_link})",
                               event_type=2, notify_admin=False)
                    else:
                        post.type = 3
                        sender("Я сделал репост, но похоже, что неверно 😔\n"
                               f"[Пост]({post_link}) {post_title_text} не прошел проверку",
                               event_type=3, notify_admin=True,
                               debug_info={
                                   'post-id': post_id,
                               })
                elif file_exists(clicker_log_dir + 'error.log'):
                    post.type = 3
                    error_message = read_from_file(clicker_log_dir + 'error.log')
                    if 'FindFailed' in error_message or 'error' in error_message:
                        if post.attempts <= 1:
                            post.type = 1
                            text = (f"Во время прокликивания [поста]({post_link}) "
                                    f"{post_title_text} "
                                    "возникла ошибка😔\n\n"
                                    "Я запущу этот пост еще раз сейчас")
                        else:
                            text = (f"Я прокликиваю [пост]({post_link}) {post_title_text} "
                                    "уже второй раз и опять возникла ошибка😔\n\n"
                                    "Я помечу его как ошибочный")
                        sender(text, event_type=3, notify_admin=True, debug_info={
                            'post-id': post_id,
                            'sikulix-stdout': sikulix.stdout
                        })
                    else:
                        notify_admin = 'На это время уже запланирован пост' not in error_message
                        sender(f"Во время прокликивания [поста]({post_link}) "
                               f"{post_title_text} возникла ошибка😔\n\n"
                               f"{error_message}",
                               event_type=3, notify_admin=notify_admin,
                               debug_info={
                                   'post-id': post_id,
                                   'sikulix-stdout': sikulix.stdout
                               })
                else:
                    post.type = 3
                    sender(f"Что-то совсем страшное произошло во время прокликивания [поста]({post_link}) "
                           f"{post_title_text}😔\n\n"
                           "Разработчик уже уведомлен",
                           event_type=3, notify_admin=True,
                           debug_info={
                               'post-id': post_id,
                               'sikulix-stdout': sikulix.stdout
                           })
        except Exception:
            post.type = 3
            logger.exception('Exception raised by run', exc_info=True)
        post.save()
        if post.type == 2:
            post.type = 3
        post.save()

        if not (Post.select().where(
                    (Post.type == added_type) & (Post.id > post_id)
        ).exists()):
            post_numbers = get_posts_number(post, ['Done', 'Error'])
            report = create_report_text(post_numbers)
            sender(report, event_type=2, notify_admin=False)

    db.close()


if __name__ == '__main__':
    run()
