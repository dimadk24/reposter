from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View


class IndexView(LoginRequiredMixin, View):
    def get(self, request):
        return redirect('script:add_post')
