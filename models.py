import os

from dotenv import load_dotenv
from peewee import *
from playhouse.db_url import connect

load_dotenv(os.path.join(os.path.dirname(os.path.abspath(__file__)), '.env'), verbose=True)
db = connect(os.environ.get('DATABASE_URL'))


class BaseModel(Model):
    class Meta:
        database = db


class AuthUser(BaseModel):
    date_joined = DateTimeField()
    email = CharField()
    first_name = CharField()
    is_active = BooleanField()
    is_staff = BooleanField()
    is_superuser = BooleanField()
    last_login = DateTimeField(null=True)
    last_name = CharField()
    password = CharField()
    username = CharField(unique=True)

    class Meta:
        table_name = 'auth_user'


class NotifyType(BaseModel):
    text = CharField()

    class Meta:
        table_name = 'script_notifytype'


class Info(BaseModel):
    access_token = CharField()
    notify_type = ForeignKeyField(column_name='notify_type_id', field='id', model=NotifyType)
    profile_dir = CharField()
    tg_id = BigIntegerField(column_name='tg_id', null=True)
    user = ForeignKeyField(column_name='user_id', field='id', model=AuthUser, unique=True)
    vk_id = BigAutoField(column_name='vk_id')

    class Meta:
        table_name = 'script_info'


class Type(BaseModel):
    text = CharField()

    class Meta:
        table_name = 'script_type'


class Post(BaseModel):
    created = DateTimeField()
    datetime = DateTimeField()
    image = CharField()
    link = CharField()
    new_post_id = CharField(column_name='new_post_id')
    public = CharField()
    text = TextField()
    type = ForeignKeyField(column_name='type_id', field='id', model=Type, null=True)
    user = ForeignKeyField(column_name='user_id', field='id', model=AuthUser)
    attempts = IntegerField()

    class Meta:
        table_name = 'script_post'


class Template(BaseModel):
    active = BooleanField()
    created = DateTimeField()
    name = CharField()
    photos = TextField()
    posts = TextField()
    publics = TextField()
    texts = TextField()
    times = TextField()
    user = ForeignKeyField(column_name='user_id', field='id', model=AuthUser)

    class Meta:
        table_name = 'script_template'
