from typing import Any

from tg_api import TgRequest

NOTIFY_TYPES = {
    'none': 1,
    'error': 2,
    'success': 3,
    'all': 4
}


class MessageSender:
    def __init__(self, bot_token: str, notify_type: int,
                 user_id: int, admin_id: int):
        self.r = TgRequest(bot_token)
        self.notify_type = notify_type
        self.user_id = user_id
        self.admin_id = admin_id

    def __call__(self, message: str, event_type: int,
                 notify_admin: bool, debug_info: Any = None):
        if self._should_notify(event_type):
            self._text_with_markdown(self.user_id, text=message)
        if notify_admin:
            self._notify_admin(message, debug_info)

    def _should_notify(self, event_type):
        return (
            (self.notify_type == NOTIFY_TYPES['all']) or
            (
                (self.notify_type == event_type) and
                (event_type in [NOTIFY_TYPES['error'], NOTIFY_TYPES['success']])
             )
        )

    def _notify_admin(self, user_message: str, debug_info: Any) -> Any:
        return self._text_with_markdown(self.admin_id,
                                        text=(
                                            f"У пользователя {self.user_id} "
                                            "возникла ошибка☠️\n\n"
                                            f"{user_message}\n\n"
                                            "Отладочная информация:\n"
                                            f"{debug_info}"))

    def _text_with_markdown(self, chat_id: int, text: str) -> Any:
        return self.r('sendMessage', chat_id=chat_id,
                      parse_mode='Markdown', text=text)
