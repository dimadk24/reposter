import hashlib

import requests
from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http.response import JsonResponse
from django.shortcuts import redirect, render
# Create your views here.
from django.views import View
from django.views.generic import TemplateView

from script.models import Info, NotifyType
from tg_api import TgRequest


class Reg(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('script:add_post')
        form = UserCreationForm()
        return render(request, 'usermanage/reg.html', {'form': form})

    def post(self, request):
        if request.user.is_authenticated:
            return redirect('script:add_post')
        form = UserCreationForm(request.POST)
        if not form.is_valid():
            return render(request, 'usermanage/reg.html', {'form': form})
        user = User.objects.create_user(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
        login(request, user)
        return redirect('script:settings')
