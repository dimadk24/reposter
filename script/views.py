import hashlib
import re
from datetime import timedelta, timezone as datetime_timezone

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone as django_timezone
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import ListView
from django.views.generic.base import RedirectView, TemplateView

from article import is_article_link
from script.datetime_parser import Parser, DatetimeParserException
from script.forms import TemplateForm
from script.models import Post, Template, Type, Info, NotifyType
from tg_api import TgRequest
from .mixins import InfoRequiredMixin

vk_redirect_uri = 'http://' + settings.DOMAIN + '/app/vk_login'
settings_url = 'http://' + settings.DOMAIN + '/app/settings'


def get_tg_hash(user):
    try:
        info_obj = Info.objects.get(user=user)
    except Info.DoesNotExist:
        return None
    vk_id = info_obj.vk_id
    access_token = info_obj.access_token
    if not (vk_id and access_token):
        return None
    hash_str = (str(vk_id) + settings.TG_HASH_SALT + access_token[:round(len(access_token) / 2)]).encode('ascii')
    return hashlib.md5(hash_str).hexdigest()


@method_decorator(ensure_csrf_cookie, name='dispatch')
class AddPostView(LoginRequiredMixin, InfoRequiredMixin, View):
    def get(self, request):
        templates = Template.objects\
            .filter(user=request.user, active=True)\
            .order_by('order_id')
        new_templates = []
        for template in templates:
            new_template = {
                'id': template.id,
                'name': template.name
            }
            new_templates.append(new_template)
        return render(request, 'script/add_post.html', {'templates': new_templates})


@method_decorator(ensure_csrf_cookie, name='dispatch')
class ListTemplatesView(LoginRequiredMixin, ListView):
    model = Template
    allow_empty = True
    template_name = 'script/template_list.html'

    def get_queryset(self):
        return Template.objects\
            .filter(user=self.request.user, active=True)\
            .order_by('order_id')


class CreateTemplateView(LoginRequiredMixin, View):
    def get(self, request):
        form = TemplateForm()
        return render(request, 'script/template.html', {'form': form})

    def post(self, request):
        template = Template(user=request.user, active=True)
        form = TemplateForm(request.POST, instance=template)
        if not form.is_valid():
            return render(request, 'script/template.html', {'form': form})
        form.save()
        return redirect('script:list_templates')


@method_decorator(ensure_csrf_cookie, name='dispatch')
class UpdateTemplateView(LoginRequiredMixin, View):
    def get(self, request, template_id):
        template = get_object_or_404(Template, pk=template_id, user=request.user, active=True)
        data = {
            'name': template.name,
            'photos': template.photos,
            'publics': template.publics,
            'posts': template.posts,
            'texts': template.texts,
            'times': template.times,
        }
        form = TemplateForm(data)
        return render(request, 'script/template.html', {'form': form, 'template_id': template_id})

    def post(self, request, template_id):
        template = Template.objects.get(user=request.user, pk=template_id, active=True)
        form = TemplateForm(request.POST, instance=template)
        if form.is_valid():
            form.save()
        return render(request, 'script/template.html', {'form': form, 'template_id': template_id})


class UpdateTemplateOrderApi(LoginRequiredMixin, View):
    def post(self, request):
        post = request.POST
        for template_id in post:
            template_order = post[template_id]
            Template.objects.filter(pk=template_id).update(order_id=template_order)
        return JsonResponse({'ok': 'ok'})


class CopyTemplateView(LoginRequiredMixin, View):
    def get(self, request, template_id):
        template = get_object_or_404(Template, pk=template_id, user=request.user, active=True)
        Template.objects.create(name='[Копия] ' + template.name,
                                photos=template.photos,
                                posts=template.posts,
                                publics=template.publics,
                                texts=template.texts,
                                times=template.times,
                                user=request.user)
        return redirect('script:list_templates')


class AddPostApi(LoginRequiredMixin, InfoRequiredMixin, View):
    def post(self, request):
        data = request.POST
        links = data.get('links')
        publics = data.get('publics')
        texts = data.get('texts')
        images = data.get('imgs')
        post_datetimes = data.get('datetimes')
        if not (links and publics and texts and images and post_datetimes):
            return JsonResponse({'error': 'Переданы не все нужные параметры'},
                                json_dumps_params={'ensure_ascii': False})
        links = links.splitlines()
        publics = publics.splitlines()
        if '\r\n' in texts:
            texts = texts.split('\r\n_end_\r\n')
        else:
            texts = texts.split("\n_end_\n")
        images = images.splitlines()
        post_datetimes = post_datetimes.splitlines()
        new_texts = []
        for text in texts:
            text = text.strip()
            new_texts.append(text)
        texts = new_texts
        if texts[-1].endswith("\r\n_end_"):
            texts[-1] = texts[-1][0: texts[-1].rfind("\r\n_end_")]
        elif texts[-1].endswith("\n_end_"):
            texts[-1] = texts[-1][0: texts[-1].rfind("\n_end_")]
        i = 0
        datetime_parser = Parser(datetime_timezone(timedelta(hours=3)))
        for post_datetime in post_datetimes:
            try:
                post_datetimes[i] = datetime_parser(post_datetime)
            except DatetimeParserException as e:
                return JsonResponse({'error': f'Неверный формат даты или времени поста №{i + 1}<br>"' + str(e) + '"<br>'
                                              'Нужен "ДД.ММ.ГГГГ ЧЧ:ММ"<br>'
                                              'Или "$<i>число</i> ЧЧ:ММ"<br>'
                                              'Где <i>число</i>: любое число сдвига даты<br>'
                                              'Например: <i>$4 12:25</i><br>'
                                              '(<i>Сегодня</i> = <i>0</i> )<br>'
                                              'На всякий случай ни один пост не был добавлен'},
                                    json_dumps_params={'ensure_ascii': False})
            i += 1
        i = 0
        regex = re.compile('((wall-|wall)\d+_\d+)|(z=article_edit-?\d+_\d+)|(vk.com/@.+)$')
        for link in links:
            match = regex.search(link)
            if not match:
                return JsonResponse({'error': 'Неверная ссылка в посте №{number}<br>'.format(number=i + 1) +
                                              'На всякий случай ни один пост не был добавлен'},
                                    json_dumps_params={'ensure_ascii': False})
            if not is_article_link(link):
                link = 'https://vk.com/' + match.group(0)
                links[i] = link
            i += 1
        i = 0
        for image in images:
            if not ('.png' in image or '.jpg' in image or '.gif' in image or '.webp' in image):
                return JsonResponse({'error': 'Неверная ссылка на картинку в посте №{number}<br>'.format(number=i + 1) +
                                              'На всякий случай ни один пост не был добавлен'},
                                    json_dumps_params={'ensure_ascii': False})
            i += 1
        i = 0
        posts_type = Type.objects.get(pk=1)
        posts = []
        for link in links:
            public = publics[i]
            text = texts[i]
            image = images[i]
            post_datetime = post_datetimes[i]
            post = Post(link=link, public=public, text=text, image=image, datetime=post_datetime,
                        user=request.user, type=posts_type, created=django_timezone.now())
            posts.append(post)
            i += 1
        Post.objects.bulk_create(posts)
        return JsonResponse({'ok': i}, json_dumps_params={'ensure_ascii': False})


class GetTemplateByIdApi(LoginRequiredMixin, View):
    def post(self, request):
        template_id = request.POST.get('id')
        if template_id is None:
            return JsonResponse({'error': 'Не передан id'}, json_dumps_params={'ensure_ascii': False})
        try:
            template = Template.objects.get(user=request.user, pk=template_id, active=True)
        except Template.DoesNotExist:
            return JsonResponse({'error': '404 Template не найден'}, json_dumps_params={'ensure_ascii': False})
        json_template = {
            'name': template.name,
            'photos': template.photos,
            'posts': template.posts,
            'publics': template.publics,
            'texts': template.texts,
            'times': template.times,
        }
        return JsonResponse({'template': json_template}, json_dumps_params={'ensure_ascii': False})


class IndexView(LoginRequiredMixin, RedirectView):
    pattern_name = 'script:add_post'


@method_decorator(ensure_csrf_cookie, name='dispatch')
class Settings(LoginRequiredMixin, TemplateView):
    template_name = 'script/settings.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['redirect_uri'] = vk_redirect_uri
        context['tg_secret'] = get_tg_hash(self.request.user)
        try:
            context['notify_type_id'] = Info.objects.get(user=self.request.user).notify_type_id - 1
        except Info.DoesNotExist:
            context['notify_type_id'] = 0
        context['text'] = self.request.GET.get('text')
        return context


class VKRedirectURI(LoginRequiredMixin, View):
    def get(self, request):
        code = request.GET.get('code', default=None)
        if not code:
            return redirect('script:settings')
        request_data = {
            'client_id': 6444581,
            'client_secret': 'A8SnxysQyncmQYB6Yf9I',
            'redirect_uri': vk_redirect_uri,
            'code': code
        }
        r = requests.post('https://oauth.vk.com/access_token', data=request_data)
        json_response = r.json()
        if 'error' in json_response:
            return render(request, 'script/settings.html', {'error': json_response['error'],
                                                            'redirect_uri': vk_redirect_uri})
        info = Info.objects.get_or_create(vk_id=json_response['user_id'], user=request.user)[0]
        info.access_token = json_response['access_token']
        info.save()
        return redirect('script:settings')


class TGApi(LoginRequiredMixin, View):
    def post(self, request):
        r = TgRequest(settings.TG_BOT_TOKEN)
        updates = r('getUpdates', offset=-100)
        for update in updates:
            if 'message' in update or 'edited_message' in update:
                msg = update.get('message') or update.get('edited_message')
                text = msg.get('text').strip()
                tg_hash = get_tg_hash(request.user)
                if text == tg_hash:
                    try:
                        info = Info.objects.get(user=request.user)
                    except Info.DoesNotExist:
                        return JsonResponse({'error': 'Вначале нужно пройти авторизацию ВК'},
                                            json_dumps_params={'ensure_ascii': False})
                    info.tg_id = msg.get('from')['id']
                    info.save()
                    message_text = """Привет, **{name}**! Я буду уведомлять тебя о работе скрипта репостов 😊\n
Управлять типами уведомлений ты можешь в [настройках]({settings_url})\n
Не пиши мне ничего, в этом чате я только отправляю уведомления 😌""".format(
                        name=msg['from']['first_name'], settings_url=settings_url)
                    r('sendMessage', chat_id=msg['from']['id'], text=message_text, parse_mode='Markdown')
                    return JsonResponse({'ok': 'ok'}, json_dumps_params={'ensure_ascii': False})
        return JsonResponse({'error': 'Сообщения с нужным текстом не найдено'},
                            json_dumps_params={'ensure_ascii': False})


class SaveApi(LoginRequiredMixin, View):
    def post(self, request):
        if 'number' not in request.POST:
            return JsonResponse({'error': 'Не передан нужный параметр'}, json_dumps_params={'ensure_ascii': False})
        number = request.POST.get('number')
        if not number:
            return JsonResponse({'error': 'Не передан нужный параметр'}, json_dumps_params={'ensure_ascii': False})
        number = int(number)
        if number not in range(0, 4):
            return JsonResponse({'error': 'Неверное значение параметра'}, json_dumps_params={'ensure_ascii': False})
        try:
            info = Info.objects.get(user=request.user)
        except Info.DoesNotExist:
            return JsonResponse({'error': 'Вначале нужно авторизироваться через ВК'},
                                json_dumps_params={'ensure_ascii': False})
        notify_type = NotifyType.objects.get(pk=number + 1)
        info.notify_type = notify_type
        info.save()
        return JsonResponse({'ok': 'ok'}, json_dumps_params={'ensure_ascii': False})


class DeleteTemplateApi(LoginRequiredMixin, View):
    def post(self, request):
        if 'number' not in request.POST:
            return JsonResponse({'error': 'Не передан нужный параметр'}, json_dumps_params={'ensure_ascii': False})
        number = request.POST.get('number')
        if not number:
            return JsonResponse({'error': 'Не передан нужный параметр'}, json_dumps_params={'ensure_ascii': False})
        number = int(number)
        try:
            template = Template.objects.get(pk=number, user=request.user, active=True)
        except Template.DoesNotExist:
            return JsonResponse({'error': '404 шаблон не найден'}, json_dumps_params={'ensure_ascii': False})
        template.active = False
        template.save()
        return JsonResponse({'ok': 'ok'}, json_dumps_params={'ensure_ascii': False})


@method_decorator(ensure_csrf_cookie, name='dispatch')
class UpdatePostView(LoginRequiredMixin, InfoRequiredMixin, View):
    def get(self, request, post_id):
        lookup_kwargs = {'pk': post_id}
        if not request.user == get_user_model().objects.get(username='DimaDK'):
            lookup_kwargs.update(user=request.user)
        post = get_object_or_404(Post, **lookup_kwargs)
        context = {
            'id': post.pk,
            'link': post.link,
            'public': post.public,
            'text': post.text,
            'image': post.image,
            'datetime': post.datetime
        }
        if post.type == Type.objects.get(pk=2):
            return render(request, 'script/working_post.html')
        else:
            return render(request, 'script/update_post.html', context)


class UpdatePostApi(View):
    def post(self, request):
        data = request.POST
        post_id = data.get('post_id')
        links = data.get('links')
        publics = data.get('publics')
        texts = data.get('texts')
        images = data.get('imgs')
        post_datetimes = data.get('datetimes')
        if not (post_id and links and publics and texts and images and post_datetimes):
            return JsonResponse({'error': 'Переданы не все нужные параметры'},
                                json_dumps_params={'ensure_ascii': False})
        links = links.splitlines()
        publics = publics.splitlines()
        if '\r\n' in texts:
            texts = texts.split('\r\n_end_\r\n')
        else:
            texts = texts.split("\n_end_\n")
        images = images.splitlines()
        post_datetimes = post_datetimes.splitlines()
        new_texts = []
        for text in texts:
            text = text.strip()
            new_texts.append(text)
        texts = new_texts
        if texts[-1].endswith("\r\n_end_"):
            texts[-1] = texts[-1][0: texts[-1].rfind("\r\n_end_")]
        elif texts[-1].endswith("\n_end_"):
            texts[-1] = texts[-1][0: texts[-1].rfind("\n_end_")]
        i = 0
        datetime_parser = Parser(datetime_timezone(timedelta(hours=3)))
        for post_datetime in post_datetimes:
            try:
                post_datetimes[i] = datetime_parser(post_datetime)
            except DatetimeParserException as e:
                return JsonResponse({'error': f'Неверный формат даты или времени поста №{i + 1}<br>' + str(e) + '<br>'
                                              'Нужен "ДД.ММ.ГГГГ ЧЧ:ММ"<br>'
                                              'Или "$<i>число</i> ЧЧ:ММ"<br>'
                                              'Где <i>число</i>: любое число сдвига даты<br>'
                                              'Например <i>$4 12:25</i>'
                                              '(<i>Сегодня</i> это <i>0</i>)<br>'
                                              'На всякий случай ни один пост не был добавлен'},
                                    json_dumps_params={'ensure_ascii': False})
            i += 1
        i = 0
        regex = re.compile('(wall-|wall)\d+_\d+$')
        for link in links:
            match = regex.search(link)
            if not match:
                return JsonResponse({'error': 'Неверная ссылка на пост в посте<br>'
                                              'На всякий случай ни один пост не был обновлен'},
                                    json_dumps_params={'ensure_ascii': False})
            link = 'https://vk.com/' + match.group(0)
            links[i] = link
            i += 1
        for image in images:
            if not ('.png' in image or '.jpg' in image or '.gif' in image or '.webp' in image):
                return JsonResponse({'error': 'Неверная ссылка на картинку в посте}<br>'
                                              'На всякий случай пост не был обновлен'},
                                    json_dumps_params={'ensure_ascii': False})
        i = 0
        posts_type = Type.objects.get(pk=1)
        for link in links:
            public = publics[i]
            text = texts[i]
            image = images[i]
            post_datetime = post_datetimes[i]
            if not Post.objects.filter(user=request.user, pk=post_id).exclude(type=Type.objects.get(pk=2)).exists():
                return JsonResponse({'error': 'Неверный id поста или пост сейчас прокликивается<br>'
                                              'На всякий случай ни один пост не был обновлен'},
                                    json_dumps_params={'ensure_ascii': False})
            Post.objects.filter(pk=post_id).update(link=link, public=public, text=text,
                                                   image=image, datetime=post_datetime,
                                                   user=request.user, type=posts_type)
        return JsonResponse({'ok': 'ok'}, json_dumps_params={'ensure_ascii': False})
