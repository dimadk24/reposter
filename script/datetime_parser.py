import re
import unittest
from datetime import datetime, timedelta, timezone


class DatetimeParserException(Exception):
    pass


class BadFormat(DatetimeParserException):
    pass


class BadVar(DatetimeParserException):
    pass


class Parser:
    def __init__(self, tz):
        self.regex = re.compile(r'\$(\d+)')
        self.tz = tz

    def __call__(self, string):
        work_string = string.strip()
        if '$' in work_string:
            result = self.regex.subn(self._convert_var_to_date, work_string)
            if not result[1] == 1:
                raise BadVar(f'Неверная переменная в {string}')
            work_string = result[0]
        try:
            return datetime.strptime(work_string, '%d.%m.%Y %H:%M').replace(tzinfo=self.tz)
        except ValueError:
            raise BadFormat(f'Неверный формат {string} (с переменными проблем нет)')

    def _convert_var_to_date(self, match):
        number = int(match.group(1))
        return (datetime.now(self.tz) + timedelta(days=number)).strftime('%d.%m.%Y')


class TestParset(unittest.TestCase):
    minsk_tz = timezone(timedelta(hours=3))
    minsk_parser = Parser(minsk_tz)
    utc_tz = timezone(timedelta(hours=0))
    utc_parser = Parser(utc_tz)

    def test_simple_time_minsk_tz(self):
        source = '24.02.2000 00:40'
        self.assertEqual(datetime(2000, 2, 24, 0, 40, tzinfo=self.minsk_tz), self.minsk_parser(source))

    def test_var0_minsk_tz(self):
        source = '$0 00:40'
        source_datetime = datetime.now(self.minsk_tz).replace(hour=0, minute=40, second=0, microsecond=0)
        self.assertEqual(source_datetime, self.minsk_parser(source))

    def test_var1_minsk_tz(self):
        source = '$1 21:30'
        source_datetime = (datetime.now(self.minsk_tz) + timedelta(days=1)) \
            .replace(hour=21, minute=30, second=0, microsecond=0)
        self.assertEqual(source_datetime, self.minsk_parser(source))

    def test_var10_minsk_tz(self):
        source = '$10 10:15'
        source_datetime = (datetime.now(self.minsk_tz) + timedelta(days=10)) \
            .replace(hour=10, minute=15, second=0, microsecond=0)
        self.assertEqual(source_datetime, self.minsk_parser(source))

    def test_simple_utc_tz(self):
        source = '24.02.2000 00:40'
        self.assertEqual(datetime(2000, 2, 24, 0, 40, tzinfo=self.utc_tz), self.utc_parser(source))

    def test_var0_utc_tz(self):
        source = '$0 00:40'
        source_datetime = datetime.now(tz=self.utc_tz).replace(hour=0, minute=40, second=0, microsecond=0)
        self.assertEqual(source_datetime, self.utc_parser(source))

    def test_var1_utc_tz(self):
        source = '$1 21:25'
        source_datetime = (datetime.now(tz=self.utc_tz) + timedelta(days=1)) \
            .replace(hour=21, minute=25, second=0, microsecond=0)
        self.assertEqual(source_datetime, self.utc_parser(source))

    def test_raises_bad_var(self):
        source = '$ 21:50'
        parser = Parser(self.minsk_tz)
        with self.assertRaises(BadVar):
            parser(source)

    def test_raises_bad_double_var(self):
        source = '$1 $0 21:50'
        parser = Parser(self.minsk_tz)
        with self.assertRaises(BadVar):
            parser(source)

    def test_raises_bad_format(self):
        source = '21/02,2000 12 45'
        parser = Parser(self.minsk_tz)
        with self.assertRaises(BadFormat):
            parser(source)


if __name__ == '__main__':
    unittest.main()
