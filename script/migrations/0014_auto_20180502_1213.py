# Generated by Django 2.0.4 on 2018-05-02 09:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('script', '0013_auto_20180501_1813'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotifyType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=15)),
            ],
        ),
        migrations.RemoveField(
            model_name='info',
            name='notify_mode',
        ),
        migrations.DeleteModel(
            name='NotifyMode',
        ),
        migrations.AddField(
            model_name='info',
            name='notify_mode',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='script.NotifyType'),
        ),
    ]
