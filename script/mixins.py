from django.shortcuts import redirect
from django.urls import reverse

from script.models import Info


class InfoRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        text = ''
        try:
            info = Info.objects.get(user=request.user)
        except Info.DoesNotExist:
            text = 'Вначале нужно войти ВК'
        if not text and not (info.vk_id and info.access_token and info.tg_id and info.notify_type):
            text = 'Для работы нужно подключить ТГ бота'
        if text:
            return redirect(reverse('script:settings') + '?text=' + text)
        else:
            return super().dispatch(request, *args, **kwargs)
