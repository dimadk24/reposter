$(document).ready(function () {
    const csrftoken = Cookies.get('csrftoken');
    $('.delete-but').click(function (e) {
        e.preventDefault();
        const li = $(this).parents('li');
        const id = li.data('id');
        const link = li.children('a');
        const name = link.text();
        swal({
            title: 'Ты уверен?',
            html: 'Удалить шаблон <span class="template-name">' + name + "</span>?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да!',
            cancelButtonText: 'Нет, отмена',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "/app/templates/deleteApi",
                    method: "POST",
                    success: function (data) {
                        if (data.ok) {
                            swal({
                                title: 'Удалено!',
                                html: 'Шаблон <span class="template-name">' + name + "</span> успешно удален",
                                type: 'success'
                            });
                            li.slideUp();
                        }
                        else
                            swal({
                                type: 'error',
                                title: 'Ошибка удаления',
                                html: data.error
                            })
                    },
                    error: function(){
                        swal({
                            type: 'error',
                            title: 'Ошибка',
                            text: 'Возникла внутреняя ошибка сервера'
                        })
                    },
                    headers: {
                        "X-CSRFToken": csrftoken
                    },
                    data: {
                        'number': id
                    }
                });
            }
        })
    });

    const templateList = document.getElementById('template_list');
    if (templateList.hasChildNodes()) {
        dragula([templateList], {
            revertOnSpill: true,
            moves: (element, container, handle) => {
                const span = handle.parentNode;
                return span.classList.contains('drag-handle');
            }
        })
            .on('drop', (element) => {
                const templatesOrder = [];
                for (const template of Array.from(templateList.children)) {
                    console.log(template);
                    templatesOrder.push({
                        id: template.getAttribute('data-id'),
                        orderId: getElementIndex(template)
                    })
                }
                saveOrder(templatesOrder)
            })
    }

    function getElementIndex(element) {
        return [...element.parentNode.children].indexOf(element) + 1;
    }

    function saveOrder(params) {
        const data = {};
        params.forEach(template => data[template.id] = template.orderId);
        $.ajax({
            url: "/app/templates/updateOrderApi",
            method: "POST",
            error: () => swal({
                type: 'error',
                title: 'Ошибка',
                text: 'Возникла ошибка сервера при сохранении порядка'
            }),
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: data
        });
    }
});