$(document).ready(function () {
    $('select#template_select').select2({
        language: "ru"
    });
    $('#template_select').on('select2:select', function (e) {
        const id = e.params.data.id;
        if (id == 0){
            $('#link_area').val('');
            $('#public_area').val('');
            $('#text_area').val('');
            $('#img_area').val('');
            $('#datetime_area').val('');
            return false;
        }
        const csrftoken = Cookies.get('csrftoken');
        $.ajax({
            url: "/app/templates/getByIdApi",
            method: "POST",
            success: function (data) {
                if (data.template){
                    $('#link_area').val(data.template.posts);
                    $('#public_area').val(data.template.publics);
                    $('#text_area').val(data.template.texts);
                    $('#img_area').val(data.template.photos);
                    $('#datetime_area').val(data.template.times);
                }
                else
                    swal({
                        type: 'error',
                        title: 'Ошибка',
                        text: data.error
                    })
            },
            error: function(){
                swal({
                    type: 'error',
                    title: 'Ошибка',
                    text: 'Возникла внутреняя ошибка сервера'
                })
            },
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: {
                'id': id
            }
        });
    });

    $('button.submit').click(function (e) {
        e.preventDefault();
        const links = $('#link_area').val().split("\n");
        const publics = $('#public_area').val().split("\n");
        let texts = $('#text_area').val().split("\n_end_\n");
        {
            let end_index = texts[texts.length-1].lastIndexOf("\n_end_");
            if (end_index !== -1)
                texts[texts.length - 1] = texts[texts.length -1].slice(0, end_index);
        }
        if (!texts[texts.length - 1])
            texts.pop();
        const imgs = $('#img_area').val().split("\n");
        const datetimes = $('#datetime_area').val().split("\n");
        const number = links.length;
        if (!(number === publics.length && number === texts.length && number === imgs.length
                && number === datetimes.length)) {
            swal({
                type: 'error',
                title: 'Неверно',
                html: `Количество ссылок на посты (${links.length}),  пабликов (${publics.length}),<br>
текстов (${texts.length}), картинок (${imgs.length}) и времени (${datetimes.length}) не равно`
            });
            return false;
        }
        for (let i = 0; i < links.length; i++){
            let result = links[i].match(/(wall-|wall)\d+_\d+$/);
            if (result !== null)
                links[i] = 'https://vk.com/' + result[0];
        }

        let data_obj = {};
        let serialize_data = $('form').serializeArray();
        for (let i = 0; i < serialize_data.length; i++){
            data_obj[serialize_data[i].name] = serialize_data[i].value
        }


        const csrftoken = Cookies.get('csrftoken');
        $.ajax({
            url: "/app/post/addApi",
            method: "POST",
            success: function (data) {
                if (data.ok)
                    swal({
                        type: 'success',
                        title: 'Успешно',
                        text: 'Добавлено постов: ' + data.ok.toString()
                    });
                else
                    swal({
                        type: 'error',
                        title: 'Ошибка',
                        html: data.error
                    })
            },
            error: function(){
                swal({
                    type: 'error',
                    title: 'Ошибка',
                    text: 'Возникла внутреняя ошибка сервера'
                })
            },
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: data_obj
        });
    })
});
