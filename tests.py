import os
import re
from datetime import datetime, timedelta, timezone
from unittest import TestCase

from dotenv import load_dotenv

from article import is_production_article_link, is_edit_article_link, \
    get_article_id_from_edit_link, create_attachments, upload_image, \
    post_wall_post_with_article, get_article_id_from_production_link, convert_article_link_to_id, \
    is_article_link, is_with_protocol
from my_functions import rstrip_list, strip_equals, convert_vk_link, resolve_link, get_main_text, \
    error_only_report_format, success_only_report_format, both_report_format, footer_text, \
    header_text, \
    create_report_text, get_owner_id_from_link
from my_exceptions import BadUserName


load_dotenv('.env')


class VkLinkConverterTest(TestCase):
    id_link_text = 'club47766862 (Читать продолжение в источнике..)'
    dog_id_text = '@' + id_link_text
    asterisk_id_text = '*' + id_link_text
    result_text = '[club47766862|Читать продолжение в источнике..]'

    def test_dog_small(self):
        self.assertEqual(convert_vk_link(self.dog_id_text), self.result_text)

    def test_dog_with_spases(self):
        text_format = "   {text}     "
        text = text_format.format(text=self.dog_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_dog_with_braces_before(self):
        text_format = "(cool text) {text}"
        text = text_format.format(text=self.dog_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_dog_with_braces_after(self):
        text_format = '{text} (cool text)'
        text = text_format.format(text=self.dog_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_dog_with_braces_before_and_after(self):
        text_format = "(cool text) {text} (very cool text)"
        text = text_format.format(text=self.dog_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_asterisk_small(self):
        self.assertEqual(convert_vk_link(self.asterisk_id_text), self.result_text)

    def test_asterisk_with_spases(self):
        text_format = "   {text}     "
        text = text_format.format(text=self.asterisk_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_asterisk_with_braces_before(self):
        text_format = "(cool text) {text}"
        text = text_format.format(text=self.asterisk_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_asterisk_with_braces_after(self):
        text_format = '{text} (cool text)'
        text = text_format.format(text=self.asterisk_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_asterisk_with_braces_before_and_after(self):
        text_format = "(cool text) {text} (very cool text)"
        text = text_format.format(text=self.asterisk_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_dog_name_public_with_braces(self):
        text_format = "(cool text) {text} (very cool text)"
        text = text_format.format(text='@vyjty_zamuzh (Читать продолжение в источнике..)')
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_asterisk_name_public_with_braces(self):
        text_format = "(cool text) {text} (very cool text)"
        text = text_format.format(text='*vyjty_zamuzh (Читать продолжение в источнике..)')
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_dog_name_public_without_spaces(self):
        text_format = "(cool text) {text} (very cool text)"
        text = text_format.format(text='@vyjty_zamuzh(Читать продолжение в источнике..)')
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_asterisk_name_public_without_spaces(self):
        text_format = "(cool text) {text} (very cool text)"
        text = text_format.format(text='*vyjty_zamuzh(Читать продолжение в источнике..)')
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_bad_user_name(self):
        text = '(dool text) *asdklfjaksdjflkasjdfbddsfgkjhdsaeiuy (cool page) (oh, seems it\'s error there)'
        self.assertEqual(convert_vk_link(text), text)

    def test_two_dogs(self):
        text_format = "some text alskdfjla {text} aldskfjalsdfjlasdkjf\n" \
                      "\n\klasdhf   {text} more text\n"
        text = text_format.format(text=self.dog_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_two_asterisks(self):
        text_format = "some text alskdfjla {text} aldskfjalsdfjlasdkjf\n" \
                      "\n\klasdhf   {text} more text\n"
        text = text_format.format(text=self.asterisk_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_dog_and_asterisk(self):
        text_format = "some text alskdfjla {dog_text} aldskfjalsdfjlasdkjf\n" \
                      "\n\klasdhf   {asterisk_text} more text\n"
        text = text_format.format(dog_text=self.dog_id_text, asterisk_text=self.asterisk_id_text)
        result_text = text_format.format(dog_text=self.result_text, asterisk_text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_three_links(self):
        text_format = "some text alskdfjla {text} aldskfjalsdfjlasdkjf\n" \
                      "\n\klasdhf   {text} more text {text}\n"
        text = text_format.format(text=self.dog_id_text)
        result_text = text_format.format(text=self.result_text)
        self.assertEqual(convert_vk_link(text), result_text)

    def test_link_with_spaces_inside(self):
        text = '@vyjty_zamuzh      (Читать продолжение в источнике..)'
        result_text = self.result_text
        self.assertEqual(convert_vk_link(text), result_text)

    def test_just_text_without_link_but_with_some_stuff(self):
        text = "@ asdflkjsfkjalklkjsa" \
               "@ asdlfkjalsdfkj" \
               "@@@ lol)" \
               "''()" \
               "))))" \
               "@" \
               "@"
        self.assertEqual(convert_vk_link(text), text)

    def test_just_text(self):
        text = 'asdflkjadlkjdsf ljdsf lkjdgklh dfldf khdfs kjhdfslkjdsgkjhlkftnN lkasdjfkaj'
        self.assertEqual(convert_vk_link(text), text)


class TestLinkResolver(TestCase):
    def test_club_link(self):
        text = 'club47766862'
        self.assertEqual(resolve_link(text), text)

    def test_page_link(self):
        text = 'page47766862'
        self.assertEqual(resolve_link(text), text)

    def test_name(self):
        text = 'dimadk_test_public_page'
        self.assertEqual(resolve_link(text), 'club160174999')

    def test_bad_name(self):
        text = 'sadlkfjalskdjfdgjhlbkcadlskfjasf'
        with self.assertRaises(BadUserName):
            resolve_link(text)


class TestRStripList(TestCase):
    def test_main(self):
        lines_list = ["test  ", "big test  ", "   another_test   "]
        result_list = ['test', 'big test', '   another_test']
        self.assertEqual(rstrip_list(lines_list), result_list)


class TestStripEquals(TestCase):
    def test_spacious_and_not_spacious(self):
        text1 = """Привет  
        тест
        тест   
        тест 
        как дела?:     
        ей"""
        text2 = """Привет
        тест
        тест
        тест
        как дела?:
        ей"""
        self.assertTrue(strip_equals(text1, text2))

    def test_not_spacious_and_not_spacious(self):
        text1 = """Привет
        тест
        тест
        супертест
        гы"""
        text2 = """Привет
        тест
        тест
        супертест
        гы"""
        self.assertTrue(strip_equals(text1, text2))

    def test_spaciout_and_spacious(self):
        text1 = """Привет  
        тест  
        тест        
        супертест  
        гыы  """
        text2 = """Привет              
        тест         
        тест            
        супертест                 
        гыы       """
        self.assertTrue(strip_equals(text1, text2))

    def test_spaces_between_words(self):
        text1 = """Привет  
        тест  тедлфвао
        тест        ывадлоп
        супертест  ывадлопывап
        гыы  """
        text2 = """Привет              
        тест тедлфвао        
        тест ывадлоп           
        супертестывадлопывап               
        гыы       """
        self.assertTrue(strip_equals(text1, text2))

    def test_false(self):
        text1 = ("Привет ()\n"
                 "тесты\n"
                 "тесты\n"
                 "тесты\n"
                 "гы")
        text2 = ("Привет ()\n"
                 "        тест\n"
                 "        тест\n"
                 "        тест\n"
                 "        гы")
        self.assertFalse(strip_equals(text1, text2))

    def test_vk_text_with_different_spaces(self):
        text1 = """11 женских привычек, которые могут испортить отношения   

Вот Вы вся такая красивая, умная, очаровательная, сексуальная и …. раздражающая! Почему же так иногда происходит?\
Многие вещи, которые кажутся нам, женщинам, абсолютно нормальными, выводят мужчин из себя.   

Знакомьтесь, вот они: привычки-раздражители:   

1. [club45693808|Читать продолжение в источнике..]"""
        text2 = """11 женских привычек, которые могут испортить отношения 

Вот Вы вся такая красивая, умная, очаровательная, сексуальная и …. раздражающая! Почему же так иногда происходит?\
Многие вещи, которые кажутся нам, женщинам, абсолютно нормальными, выводят мужчин из себя. 

Знакомьтесь, вот они: привычки-раздражители: 

1.[club45693808|Читать продолжение в источнике..]"""
        self.assertTrue(strip_equals(text1, text2))


class GetMainTextTest(TestCase):
    def test_error_only(self):
        response = get_main_text(0, 1)
        self.assertEqual(response, error_only_report_format)

    def test_success_only(self):
        response = get_main_text(1, 0)
        self.assertEqual(response, success_only_report_format)

    def test_both(self):
        response = get_main_text(1, 1)
        self.assertEqual(response,
                         both_report_format.format(success_number=1, error_number=1))

    def test_shit(self):
        response = get_main_text(0, 0)
        self.assertEqual(response, 'Странная ошибка с подсчетом постов')


class CreateReportTest(TestCase):
    def test_simple(self):
        numbers = [1, 1]
        result = (header_text + "\n" +
                  both_report_format.format(success_number=numbers[0],
                                            error_number=numbers[1]) +
                  "\n" + footer_text.format(all_number=sum(numbers)))
        self.assertEqual(create_report_text(numbers), result)


class GetPublicFromLinkTest(TestCase):
    def test_user(self):
        link = 'vk.com/wall123_3241'
        self.assertEqual(get_owner_id_from_link(link), 123)

    def test_public(self):
        link = 'vk.com/wall-123_345345'
        self.assertEqual(get_owner_id_from_link(link), -123)


class IsProductionArticleTestCase(TestCase):
    def test_article(self):
        article = 'https://vk.com/@targethunter-iz-kuedy'
        self.assertTrue(is_production_article_link(article))

    def test_another_article(self):
        article = 'https://vk.com/@targethunter-rikanutye-kvesty'
        self.assertTrue(is_production_article_link(article))

    def test_another_article_link(self):
        article = 'vk.com/@targethunter-rikanutye-kvesty'
        self.assertTrue(is_production_article_link(article))

    def test_not_article(self):
        wall_post = 'https://vk.com/wall-131101936_65134'
        self.assertFalse(is_production_article_link(wall_post))


class IsEditArticleTestCase(TestCase):
    def test_edit_article(self):
        article = 'https://vk.com/smm_automation?z=article_edit-169568508_6595'
        self.assertTrue(is_edit_article_link(article))

    def test_not_edit_article(self):
        not_edit_article = 'vk.com/@targethunter-rikanutye-kvesty'
        self.assertFalse(is_edit_article_link(not_edit_article))


class ConvertArticleLinkToVkIdTestCase(TestCase):
    def test_one(self):
        article = 'https://vk.com/smm_automation?z=article_edit-169568508_6595'
        vk_id = 'article-169568508_6595'
        self.assertEqual(vk_id, get_article_id_from_edit_link(article))


class CreateAttachmentsTestCase(TestCase):
    def test_one(self):
        article = 'article-169568508_6595'
        image = 'photo-1_1'
        self.assertEqual('article-169568508_6595,photo-1_1',
                         create_attachments(article, image))


class UploadImageTestCase(TestCase):
    def test_one_image(self):
        with open('test_image.jpg', 'rb') as file:
            access_token = os.environ.get('TEST_ACCESS_TOKEN')
            group_id = 160174999
            new_photo_id = upload_image(access_token, file, group_id)
        is_matched = bool(re.match('^photo-?\d+_\d+$', new_photo_id))
        self.assertTrue(is_matched)


class OverallWorkflowTestCase(TestCase):
    def test_with_image(self):
        access_token = os.environ.get('TEST_ACCESS_TOKEN')
        group_id = 160174999
        message = 'test'
        article_id = '-169568508_6595'
        date = datetime.now(timezone(timedelta(hours=3))) + timedelta(hours=1)
        image_path = 'test_image.jpg'
        new_post_id = post_wall_post_with_article(access_token, group_id, message, image_path,
                                                  article_id, date)
        is_matched = bool(re.match('^-?\d+_\d+$', new_post_id))
        self.assertTrue(is_matched)

    def test_without_image(self):
        access_token = os.environ.get('TEST_ACCESS_TOKEN')
        group_id = 160174999
        message = 'test'
        article_id = '-169568508_6595'
        date = datetime.now(timezone(timedelta(hours=3))) + timedelta(hours=1)
        image_path = ''
        new_post_id = post_wall_post_with_article(access_token, group_id, message, image_path,
                                                  article_id, date)
        is_matched = bool(re.match('^-?\d+_\d+$', new_post_id))
        self.assertTrue(is_matched)


class GetArticleIdTestCase(TestCase):
    def test_one(self):
        production_link = 'https://vk.com/@smm_automation-lead-source'
        expected_article_id = 'article-169568508_6595'
        actual_article_id = get_article_id_from_production_link(production_link)
        self.assertEqual(expected_article_id, actual_article_id)

    def test_two(self):
        production_link = 'https://vk.com/@smm_automation-stop-shorting-links'
        expected_article_id = 'article-169568508_6540'
        actual_article_id = get_article_id_from_production_link(production_link)
        self.assertEqual(expected_article_id, actual_article_id)

    def test_without_protocol(self):
        production_link = 'vk.com/@smm_automation-stop-shorting-links'
        expected_article_id = 'article-169568508_6540'
        actual_article_id = get_article_id_from_production_link(production_link)
        self.assertEqual(expected_article_id, actual_article_id)


class ConvertArticleLinkToIdTestCase(TestCase):
    def test_edit_link_with_protocol(self):
        edit_link = 'https://vk.com/smm_automation?z=article_edit-169568508_6595'
        expected = 'article-169568508_6595'
        actual = convert_article_link_to_id(edit_link)
        self.assertEqual(expected, actual)

    def test_prod_link_with_protocol(self):
        production_link = 'https://vk.com/@smm_automation-stop-shorting-links'
        expected = 'article-169568508_6540'
        actual = convert_article_link_to_id(production_link)
        self.assertEqual(expected, actual)

    def test_edit_link_without_protocol(self):
        edit_link = 'vk.com/smm_automation?z=article_edit-169568508_6595'
        expected = 'article-169568508_6595'
        actual = convert_article_link_to_id(edit_link)
        self.assertEqual(expected, actual)

    def test_prod_link_without_protocol(self):
        production_link = 'vk.com/@smm_automation-stop-shorting-links'
        expected = 'article-169568508_6540'
        actual = convert_article_link_to_id(production_link)
        self.assertEqual(expected, actual)


class IsArticleLinkTestCase(TestCase):
    def test_prod_article_link(self):
        link = 'https://vk.com/@smm_automation-stop-shorting-links'
        self.assertTrue(is_article_link(link))

    def test_edit_article_link(self):
        link = 'https://vk.com/smm_automation?z=article_edit-169568508_6595'
        self.assertTrue(is_article_link(link))

    def test_not_article(self):
        link = 'https://vk.com/wall-1_1'
        self.assertFalse(is_article_link(link))


class WithProtocolLinkTestCase(TestCase):
    def test_with_https(self):
        self.assertTrue(is_with_protocol('https://vk.com/'))

    def test_with_http(self):
        self.assertTrue(is_with_protocol('http://vk.com/'))

    def test_false(self):
        self.assertFalse(is_with_protocol('vk.com/'))
