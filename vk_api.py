from typing import Dict, Any

import requests


class VkException(Exception):
    pass


class UnknownVkException(VkException):
    pass


RETRY = -1
API_URL = 'https://api.vk.com/method/'
RETRY_ERRORS = {
    6: 'Слишком много запросов в секунду',
    9: 'Слишком много однотипных действий'
}
STOP_ERRORS = {
    1: 'Произошла неизвестная для ВК ошибка.',
    5: 'Авторизация не удалась. Нужно получить новый access token',
    10: 'Произошла внутренняя ошибка сервера ВК.',
    29: 'Достигнут количественный лимит на вызов метода',
    203: 'Доступ к группе запрещён',
    224: 'Слишком много рекламных постов.',
    600: 'Нет прав на выполнение данных операций с рекламным кабинетом',
    601: 'Отказано в доступе. Вы превысили ограничение на количество запросов за день. Попробуйте позже.',
    603: 'Произошла ошибка при работе с рекламным кабинетом'
}


class VkRequest:
    def __init__(self, access_token, version):
        self.access_token = access_token
        self.version = version

    def __call__(self, relative_url, **kwargs):
        requests_params = self._append_const_data_to_dict(kwargs)
        response = self._perform_request(API_URL + relative_url, requests_params)\
            .json()
        result = self._handle_response(response)
        return result if not result == RETRY\
            else self.__call__(relative_url, **kwargs)

    def _handle_error(self, error_code, error_msg):
        if error_code in RETRY_ERRORS:
            return RETRY
        elif error_code in STOP_ERRORS:
            raise VkException(STOP_ERRORS[error_code])
        raise UnknownVkException(f'Unknown VK error happened: {error_msg}.')

    def _handle_response(self, response):
        if 'error' in response:
            response_error = response['error']
            return self._handle_error(response_error['error_code'],
                                      response_error['error_msg'])
        else:
            return response['response']

    def _perform_request(self, url: str, params: Dict) -> Any:
        try:
            response = requests.post(url, data=params)
        except requests.RequestException as e:
            raise VkException(f'Error with VK connection: {str(e)}.') from e
        self._check_response_status(response.status_code)
        return response

    def _check_response_status(self, code):
        if not code == requests.codes.ok:
            raise VkException(f'Error with VK server response code ({code}),'
                              'try perform request later.')

    def _append_const_data_to_dict(self, kwargs_dict: Dict) -> Dict:
        kwargs_dict.update(access_token=self.access_token, v=self.version)
        return kwargs_dict
